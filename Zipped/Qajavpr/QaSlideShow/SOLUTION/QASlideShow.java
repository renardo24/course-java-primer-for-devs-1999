/*
	This simple extension of the java.awt.Frame class
	contains all the elements necessary to act as the
	main window of an application.
 */

//Original code written by Dafydd Hughes,
//Dec-1997


import java.awt.*;


public class QASlideShow extends QAFrame
{
	static public void main(String args[])
	{
	    //Create a new slide show object
	    QASlideShow slideShow;
	    slideShow=new QASlideShow();
	    slideShow.show();

        //QA Add your code here
        //slideShow.moveFirst();
    	//slideShow.moveNext();
    	//slideShow.moveNext();
    	//slideShow.moveNext();
       	//slideShow.moveNext();
       	
       	slideShow.animateImage();
       	
	}
}

