




//******************WARNING************************************
//   Before using this code you will need to add the following
//     two lines to the top of your program.
//
//	import java.io.DataInputStream;
//	import java.io.IOException;
//*************************************************************

//QA helper function that waits for the return key to be pressed

    static void QAWaitForTheReturnKey ()
    {
        try 
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

