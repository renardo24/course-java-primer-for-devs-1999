import java.io.DataInputStream;
import java.io.IOException;

//
//
// TextType
//
//
public class TextType
{
	public static void main(String Args[])
	{
	// declare a separator character, initialize to '/'
	    char separator = 45; // demo of implicit conversion
		System.out.println("Current separator: "  + separator);

	// Set new separator character e.g. '-'

		separator = '-';

// Question 1.
	// declare 3 strings for the day, month and year



		    // 2000 compliance later on!!

    // invoke the QA helper methods
    
	// define and initialise a string with the day, month and year



	// display the new string together with how many characters it contains




// Question 2.
	// declare another string to hold a date which will be year 2000 compliant


	// set the value to be the same as the old date but with the century inserted


	// display  the new date together with how many characters it contains

    // all done
        QAWaitForTheReturnKey ();
	}

    //QA helper function to allow input of weight
    static String QAGetYear()
    {
        String s=new String ("");
        System.out.print ("Please enter the year: ");
        try
        {

            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
        }
        catch (IOException err )
        {

        }
        return s;
    }

    static String QAGetMonth()
    {
        String s=new String ("");
        System.out.print ("Please enter the month: ");
        try
        {

            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
        }
        catch (IOException err )
        {

        }
        return s;
    }

    static String QAGetDay()
    {
        String s=new String ("");
        System.out.print ("Please enter the day: ");
        try
        {

            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
        }
        catch (IOException err )
        {

        }
        return s;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try 
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }


}

