import java.io.DataInputStream;
import java.io.IOException;

public class Baggage
{
	//Member attributes  - private discussed in a later chapter
    private	int totalWeight=0;
    private int totalNoOfBags=0;
	
	static public void main(String args[])
	{
        Baggage b;
        b=new Baggage();

        b.recordPayload();

        QAWaitForTheReturnKey ();
    }


    void recordPayload()
    {
        int bagWeight=0;
        bagWeight=QAGetBagWeight();
                
        while (bagWeight!=-1)
        {
            if ((totalWeight + bagWeight) > 1000)
            {
                System.out.println ("Warning, the cargo is too heavy!");
                break;
            }
            
            totalNoOfBags++;
            totalWeight+=bagWeight;
            bagWeight=QAGetBagWeight();
        }

        //Ouput the results
        System.out.println ("\n\n------------------------------------");
        System.out.println ("Total No Of Bags : " + totalNoOfBags);
        System.out.println ("Total Weight : " + totalWeight);
        System.out.println ("------------------------------------");
    }


    //QA helper function to allow input of weight
    static int QAGetBagWeight()
    {
        System.out.print ("Please enter bag weight: ");
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try 
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }



}