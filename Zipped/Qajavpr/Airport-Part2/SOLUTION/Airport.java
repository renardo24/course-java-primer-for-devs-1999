import java.io.DataInputStream;
import java.io.IOException;

class Airport
{
    static public void main(String args[])
	{
        Passenger boeing747;
        boeing747=new Passenger();
        boeing747.takeOff();
        boeing747.climbTo (10000);


        Fighter F22;
        F22=new Fighter();
        F22.takeOff();
        F22.climbTo (10000);
        F22.fireMissile();

    

        //Wait for returnh key to be pressed
        QAWaitForTheReturnKey ();
    }
    
    
   
    

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}


class Aircraft
{


    //Attributes
    private int altittude;

    void takeOff ()
    {
        System.out.println ("Roger, brakes off");
        System.out.println ("Full power, here we go!");
    }
    
    void climbTo (int alt)
    {
        System.out.println ("Climbing to : " + alt );
        altittude = alt;
    }
}


class Fighter extends Aircraft
{
    public void fireMissile ()
    {
        System.out.println ("Fox 4.  Missile away!");
    }

    void takeOff ()
    {
        System.out.println ("Roger, brakes off");
        System.out.println ("Full power, here we go!");
        System.out.println ("Arming weapons load");
    }


}

class Passenger extends Aircraft
{
    void takeOff()
    {
        System.out.println ("Please fasten your seatbelts");
        super.takeOff();
    }
}
