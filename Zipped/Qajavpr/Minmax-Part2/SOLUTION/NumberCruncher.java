import java.io.DataInputStream;
import java.io.IOException;

public class NumberCruncher
{
	static public void main(String args[])
	{
	    //Create a new number cruncher object
        NumberCruncher c;
        c=new NumberCruncher();
 
        // Create two ints
        int num1=0;
        int num2=0;

        //Get the user to input two numbers
        num1=QAGetNo();
        num2=QAGetNo();
 
        // invoke printEvens method using num1 and num2 as arguments
        c.printEvens(num1, num2);  

        // Optional bit
        // int outputDisplayed = c.printEvensAndReturn(num1, num2);
        
        // System.out.println(outputDisplayed + " integers output\n");

        //Wait for user to hit the return key
        QAWaitForTheReturnKey();
    }


    void whichIsTheBigger ()
    {
        int num1=0;
        int num2=0;

        //Get the user to input two numbers
        num1=QAGetNo();
        num2=QAGetNo();

        //Make the comparison between the two numbers
        if (num1>num2)
        {
            System.out.println ("num1 is bigger than num2\n");
        }
        else if (num1<num2)
        {
            System.out.println ("num2 is bigger than num1\n");
        }
        else
        {
            System.out.println ("Both numbers are equal\n");
        }
    }

    // define the method printEvens here
    void printEvens (int num1, int num2)    
    {    
    	
    	// create two variables to hold the 
    	// starting and finishing numbers
    	
    	int start = num1;  // guess that num1 is smaller
    	int finish = num2; // than or equal to num2
    	
    	if (num1 > num2)
    	{
    	    start = num2;
    	    finish = num1;
    	}
    	
    	while(start <= finish)
		{
			if(start % 2 == 0)
			{
				System.out.print(start + "\n");
			}
			start++;
		}


    }
    
    // printEvensAndReturn needs a counter to
    // maintain the number of integers that are output
    
    int printEvensAndReturn (int num1, int num2)    
    {    
    	
    	// create two variables to hold the 
    	// starting and finishing numbers
    	
    	int start = num1;  // guess that num1 is smaller
    	int finish = num2; // than or equal to num2
    	
    	if (num1 > num2)
    	{
    	    start = num2;
    	    finish = num1;
    	}
    	
    	int count = 0;
    	while(start <= finish)
		{
			if(start % 2 == 0)
			{
				System.out.print(start + "\n");
				count++;
			}
			start++;
		}

        return count;  // back to method caller
    }

    //QA Helper function that accepts input from the user
    static int QAGetNo()
    {
        System.out.print ("Please enter a number: ");
        try
        {

            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}