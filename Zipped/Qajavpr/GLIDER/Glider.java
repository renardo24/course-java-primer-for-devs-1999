public class Glider
{
	static public void main(String args[])
	{
	    //Create a new slide show object
	    Glider myGlider;
	    myGlider=new Glider();

        myGlider.climb();
        myGlider.turnRight();
        myGlider.dive();

        //QA Add call to turnLeft method here!


        //Pause for a key press
        QAWaitForReturnKeyPress();

    }

    void turnRight ()
    {
        System.out.println ("Glider is now Turning right\n");
    }
    
    void climb ()
    {
        System.out.println ("Glider is now Climbing\n");
    }
    
    void dive ()
    {
        System.out.println ("Glider is now Diving\n");
    }

    //QA Add turnLeft method here!



    static void QAWaitForReturnKeyPress ()
    {
        //Simple method that waits for a key press
        System.out.println ("Please press the return key to exit.");
        try
        {
            System.in.read(); 
        }
        catch (java.io.IOException io)
        {
        }
    }
    
}



