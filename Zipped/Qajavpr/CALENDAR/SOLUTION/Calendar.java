import java.io.DataInputStream;
import java.io.IOException;

public class Calendar
{
	static public void main(String args[])
	{
        Calendar c;
        c=new Calendar();

        int year=0;
        boolean bIsLeapYear;

        //Get year
        while (year!=-1)
        {

            year=QAGetYear();

            bIsLeapYear=c.isLeapYear (year);

            if (bIsLeapYear)
            {
                System.out.println ("Yes, that is a leap year");
            }
            else
            {
                System.out.println ("No, that is not a leap year");
            }
        }

        QAWaitForTheReturnKey ();

    }

    //One possible solution
    boolean isLeapYear (int year)
    {
        if (((year % 4)==0) && (((year % 400) == 0)||((year % 100 !=0))))
            return true;
        return false;
    }

    //Another possible solution
    /*
    boolean isLeapYear (int year)
    {
        boolean bDivBy4=false;
        boolean bNotDivBy100=false;
        boolean bDivBy400=false;

        if ((year % 4)==0)
            bDivBy4=true;

        if ((year % 400)==0)
            bDivBy400=true;

        if ((year % 100)!=0)
            bNotDivBy100=true;

        if (bDivBy4 && (bDivBy400 || bNotDivBy100))
            return true;


        return false;
    }
    */


     //QA helper function to allow input of year
    static int QAGetYear()
    {
        System.out.print ("Please enter a year: ");
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}