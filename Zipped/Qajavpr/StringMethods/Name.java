import java.io.DataInputStream;
import java.io.IOException;


//
//
// Name
//
//
public class Name
{
	private String fullName;
	
	public static void main(String []Args)
	{
        Name name = new Name();
		int numberOfBarrels = 3;

		char connector = '-';  // Double Barreled with hyphen

		String firstName = "Phillip";
		String lastPart = "Ffolks";

        // Call buildName to produce, e.g.
        // Phillip Ffolks-Ffolks-Ffolks
        // call need firstName, lastPart, numberOfBarrels and connector
		name.buildName(firstName, lastPart, numberOfBarrels, connector);

        System.out.println();
		System.out.println(firstName + "'s full name is " + name.getName());

        QAWaitForTheReturnKey();
	}


	private void buildName(String firstBit, String lastBit, int length, char joiner)
	{
	    StringBuffer buildString = new StringBuffer(firstBit + ' ' + lastBit);

		int index = 0;
        char between = 0;
		while (index < length -1 )
		{
			buildString.append(joiner + lastBit);
			index ++;
		}

        // place into private attribute
		fullName = buildString.toString();

	}
    public String getName()
    {
        return fullName;
    }
    
    
    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }


}

