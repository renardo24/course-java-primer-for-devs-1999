import java.io.DataInputStream;
import java.io.IOException;


/*
	A basic Java class stub for a Win32 Console Application.
 */


public class SimplCon {

	public SimplCon () {

	}

    static public void main(String args[]) {
        System.out.println("Hello World");
        QAWaitForTheReturnKey ();
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

 

}



