import java.io.DataInputStream;
import java.io.IOException;

public class NumberCruncher
{
	static public void main(String args[])
	{
	    //Create a new number cruncher object
        NumberCruncher c;
        c=new NumberCruncher();
      
        // Create two ints
        int num1=0;
        int num2=0;

        //Get the user to input two numbers
        num1=QAGetNo();
        num2=QAGetNo();
 
        // invoke printEvens method using num1 and num2 as arguments
 

        //Wait for user to hit the return key
        QAWaitForTheReturnKey();
    }


    void whichIsTheBigger ()
    {
        int num1=0;
        int num2=0;

        //Get the user to input two numbers
        num1=QAGetNo();
        num2=QAGetNo();

        //Make the comparison between the two numbers
        if (num1>num2)
        {
            System.out.println ("num1 is bigger than num2\n");
        }
        else if (num1<num2)
        {
            System.out.println ("num2 is bigger than num1\n");
        }
        else
        {
            System.out.println ("Both numbers are equal\n");
        }
    }

    // define the method printEvens here, 
    // remeber it takes two integer parameters




    //QA Helper function that accepts input from the user
    static int QAGetNo()
    {
        System.out.print ("Please enter a number: ");
        try
        {

            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}