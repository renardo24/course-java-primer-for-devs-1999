import java.io.DataInputStream;
import java.io.IOException;

public class NumberCruncher
{
	static public void main(String args[])
	{
	    //Create a new number cruncher object
        NumberCruncher c;
        c=new NumberCruncher();

        c.whichIsTheBigger();

        //Wait for user to hit the return key
        QAWaitForTheReturnKey();
    }


	
	void whichIsTheBigger()
	{
        //Two int varibles set to 0
        int num1 = 0;
        int num2 = 0;
    
        //Allow the user to enter two numbers
        num1 = QAGetNo();
        num2 = QAGetNo();
    
        // Compare num1 against num2
        if (num1 > num2)
        {
            System.out.println ("num1 is greater than num2\n");
        }    
        else if (num1 < num2)
        {
        System.out.println ("num1 is smaller than num2\n");
        }    
        else
        {
            System.out.println ("Both numbers are equal!\n");
        }    
    }



    //QA Add your implementation for whichIsTheBigger here












    //QA Helper function that accepts input from the user
    static int QAGetNo()
    {
        System.out.print ("Please enter a number: ");
        try
        {

            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}