import java.io.DataInputStream;
import java.io.IOException;

class Airport
{

   
    static public void main(String args[])
	{
        // I am calling the constructer        
        Aircraft aPlane = new Aircraft("Boeing");
        aPlane.takeOff();
                
        //  when you have created your Aircraft properly
        // try to assign a value to the altitude attribute 
        // e.g. aPlane.altitude=10000;
        aPlane.climbTo(10000);

        //Wait for return key to be pressed
        QAWaitForTheReturnKey ();

    }




    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}


class Aircraft
{
    //Attributes
    private int altitude;
    public String name;
    
    void takeOff ()
    {
        System.out.println ("Roger, brakes off");
        System.out.println ("Full power, here we go!");
    }

    void climbTo (int otheraltitude)
    {
        altitude = otheraltitude;
        System.out.println ("This is our new altitude: "+altitude+" !!!\n");
    }

    //Example of a constructer
    public Aircraft (String theName)
	{
        name = theName;
        System.out.println ("The company is called: "+theName+" !!!\n");
    }



}
