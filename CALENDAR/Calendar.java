import java.io.DataInputStream;
import java.io.IOException;

public class Calendar
{
	static public void main(String args[])
	{
        Calendar c;
        c=new Calendar();

        int year=0;
        boolean bIsLeapYear;

        //QA Add code here to input the year 
        while (year != -1)
        {
            year=QAGetYear(); 
            
        //QA Add code here to call isLeapYear
            if (c.isLeapYear(year))
            {
                System.out.println ("This is a Leap Year!\n");
            }
            else
            {
                System.out.println ("This is definitely NOT a Leap Year!\n");
            }    
        }    
        //QA Add code here to output if year is 
        //or is not a leap year

        QAWaitForTheReturnKey ();

    }

    //One possible solution
    boolean isLeapYear (int year)
    {
        //QA Add your leap year logic here
        if (((year%4)==0) && (((year%400)!=0)||((year%100)!=0)))
        return true;
        return false;
    }

    //QA helper function to allow input of year
    static int QAGetYear()
    {
        System.out.print ("Please enter a year: ");
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }
}