import java.io.DataInputStream;
import java.io.IOException;

public class Baggage
{
	//Member attributes  - private discussed in a later chapter
    private	int totalWeight=0;
    private int totalNoOfBags=0;
	
	static public void main(String args[])
	{
        Baggage b;
        b=new Baggage();

        b.recordPayload();

        QAWaitForTheReturnKey ();
    }


    void recordPayload()
    {
        //QA Add code to loop and sum up weights
        int bagweight = 0;
        bagweight=QAGetBagWeight();
        
        while (bagweight != -1)
        {
            if (bagweight >= 1000)
            {
                System.out.print ("This bag is far too heavy. It must be rejected!\n");
                //bagweight = QAGetBagWeight();
                break;
            }
            else
            {
                totalWeight = totalWeight + bagweight;
                totalNoOfBags = totalNoOfBags + 1;
                bagweight = QAGetBagWeight();
            }    
        }    
        
        //QA Ouput the results
        System.out.print ("The total weight is "+totalWeight+" and the total number of bags is "+totalNoOfBags+ ".\n");
    }


    //QA helper function to allow input of weight
    static int QAGetBagWeight()
    {
        System.out.print ("Please enter bag weight: ");
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Integer No=new Integer(s);
            return No.intValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try 
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }



}