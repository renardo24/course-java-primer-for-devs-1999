import java.io.DataInputStream;
import java.io.IOException;

//
//
// DataType
//
//
public class DataType
{

	public static void main (String args[])
	{
// Question 1.
	// declare doubles for price1, price2, discount and total
    double price1, price2, discount, total = 0.0 ;

	// declare a constant for working out prices inclusive of VAT
	double VAT = 1.175;

	// declare integers for pound, pence and a counter
    int pounds, pence, count = 1;

// Question2.
	// Set the first price and add to total;
    System.out.print ("Please enter the first price ");
	price1 = QAGetPrice();
	total += price1;

	// Set the second price and add to total;
    System.out.print ("Please enter the second price ");
	price2 = QAGetPrice();
	total += price2;

	// Set the discount and subtract from total;
    System.out.print ("Please enter the discount ");
	discount = QAGetPrice();
	total -= discount;

// Question 3.
	// display the formatted invoice using both prices, discount and total
	// show the invoice line number at the start of each line using a counter
	System.out.println();
	System.out.println("  INVOICE");
	System.out.println(count++ + "  Entree ............. " + price1);
	System.out.println(count++ + "  Main Course ........ " + price2);
	System.out.println(count++ + "  Discount ........... " + discount);
	System.out.println(count++ + "  SubTotal ........... " + total);

// Optional Question
	// calculate total including VAT
	total *= VAT;

	// calculate integer values for the number of pounds and pence in
	// the total. Explicit casts can be used to suppress compiler warnings
    pounds = (int)total;

    // We need brackets around the maths bit, otherwise the (int) cast
    // causes the (total-pounds) part to always be 0, i.e. the cast takes
    // precedence over the *100
	pence  = (int)((total - pounds) * 100);

	// display a  sentence stating the total bill in pounds and pence
	System.out.print("The total amount including VAT is " + pounds + " pounds");
	System.out.println(" and " + pence + " pence");

    QAWaitForTheReturnKey ();
	}


    //QA Provided helper function to allow
    //keyboard input of price
    static double QAGetPrice()
    {
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Double price;
            price=new Double (s);
            return price.doubleValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }





    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }


}

