import java.io.DataInputStream;
import java.io.IOException;

//
//
// DataType
//
//
public class DataType
{
    
    
	public static void main(String args[])
	{
// Part 1
	// declare doubles for price1, price2, discount and total
       double price1, price2, discount, total = 0.00;
     

	// declare a constant for working out prices inclusive of VAT
       double VAT = 1.175;

	// declare integers for pound, pence and a counter
       int pounds, pence, count = 1;

// Part 2
    // Prompt the user to enter the price

    
// Part 3
	// Set the first price and add to total;
    System.out.print ("Please enter the price of the entree: ");    
    price1 = QAGetPrice();
    total = total + price1;

	// Set the second price and add to total;
    System.out.print ("Please enter the price of the main course: ");    
    price2 = QAGetPrice();
    total = total + price2;

	// Set the discount and subtract from total;
    System.out.print ("Please enter the discount: ");    
    discount = QAGetPrice();
    total = total - discount;

// Part 4
	// display the formatted invoice using both prices, discount and total
	// show the invoice line number at the start of each line using a counter
    System.out.println();
    System.out.println ("INVOICE FOR EXTRAVAGANT DINNER");    
    System.out.println ("------------------------------");    
    System.out.println (count   +"  Entree..............." + price1);
    System.out.println (++count +"  Main Course.........." + price2);
    System.out.println (++count +"  Discount............." + discount);
    System.out.println (++count +"  Subtotal............." + total);



// Optional Question
	// calculate total including VAT
    total = total * VAT;


	// calculate integer values for the number of pounds and pence in
	// the total. Explicit casts can be used to suppress compiler warnings
    pounds = (int)total;
    pence = (int)(100*(total - pounds));

	// display a  sentence stating the total bill in pounds and pence
    System.out.println();
    System.out.println ("The total amount including VAT is " +pounds+ " pounds and "+pence+" pence.");       


        QAWaitForTheReturnKey ();
	}



    //QA Provided helper function to allow
    //keyboard input of price
    static double QAGetPrice()
    {
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Double price;
            price=new Double (s);
            return price.doubleValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }





    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try 
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }
}

