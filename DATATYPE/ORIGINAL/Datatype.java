import java.io.DataInputStream;
import java.io.IOException;

//
//
// DataType
//
//
public class DataType
{


	public static void main(String args[])
	{
// Part 1
	// declare doubles for price1, price2, discount and total


	// declare a constant for working out prices inclusive of VAT


	// declare integers for pound, pence and a counter


// Part 2
    // Prompt the user to enter the price


// Part 3
	// Set the first price and add to total;



	// Set the second price and add to total;



	// Set the discount and subtract from total;



// Part 4
	// display the formatted invoice using both prices, discount and total
	// show the invoice line number at the start of each line using a counter







// Optional Question
	// calculate total including VAT



	// calculate integer values for the number of pounds and pence in
	// the total. Explicit casts can be used to suppress compiler warnings



	// display a  sentence stating the total bill in pounds and pence



        QAWaitForTheReturnKey ();
	}



    //QA Provided helper function to allow
    //keyboard input of price
    static double QAGetPrice()
    {
        try
        {
            String s;
            DataInputStream keyboard;
            keyboard=new DataInputStream (System.in);

            s=keyboard.readLine();
            Double price;
            price=new Double (s);
            return price.doubleValue();
        }
        catch (IOException err )
        {

        }
        return 0;
    }





    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }
}

