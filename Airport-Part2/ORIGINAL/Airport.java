import java.io.DataInputStream;
import java.io.IOException;

class Airport
{
    static public void main(String args[])
	{

        //Wait for return key to be pressed
        QAWaitForTheReturnKey ();
    }
    
    
   
    

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}


class Aircraft
{
    //Attributes
    private int altittude;

    void takeOff ()
    {
        System.out.println ("Roger, brakes off");
        System.out.println ("Full power, here we go!");
    }
    
    void climbTo (int alt)
    {
        System.out.println ("Climbing to : " + alt );
        altittude = alt;
    }
}


