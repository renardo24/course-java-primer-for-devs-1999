import java.io.DataInputStream;
import java.io.IOException;

class Airport
{
    static public void main(String args[])
	{
        Fighter F22;
        F22 = new Fighter();
        F22.takeOff();
        F22.climbTo(50000);
        F22.fireMissile();
        
        Passenger Boeing747;
        Boeing747 = new Passenger();
        Boeing747.takeOff();
        Boeing747.climbTo(20000);
        //Boeing747.fireMissile();

        //Wait for return key to be pressed
        QAWaitForTheReturnKey ();
    }
    
   
   
    

    //QA helper function that waits for the return key to be pressed
    static void QAWaitForTheReturnKey ()
    {
        try
        {
            System.out.println ("Please hit the return key to exit");
            System.in.read ();
        }
        catch (IOException err )
        {
        }
    }

}


class Aircraft
{
    //Attributes
    private int altittude;

    void takeOff ()
    {
        System.out.println ("Roger, brakes off");
        System.out.println ("Full power, here we go!");
    }
    
    void climbTo (int alt)
    {
        System.out.println ("Climbing to : " + alt );
        altittude = alt;
    }
}



class Fighter extends Aircraft
{
    void fireMissile ()
    {
        System.out.println ("Fox 4.    Missile away!");
    }

    void takeOff ()
    {
        System.out.println ("Let's go and destroy Britain!");
    }

}

class Passenger extends Aircraft
{
    
    void takeOff ()
    {
        System.out.println ("Please fasten your seatbelts");
        super.takeOff();
    }

}
